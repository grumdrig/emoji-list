#!/usr/bin/python

import sys, os, re

# The Emoji characters are displayed in the CharacterPalette app. It
# contains all the data we'll need, here:
apath = '/System/Library/Input Methods/CharacterPalette.app/Contents/Resources'

#
# Read the mapping from character codes to names into cdb
#

import json
import sqlite3
db = sqlite3.connect(os.path.join(apath, 'CharacterDB.sqlite3'),
                     detect_types=sqlite3.PARSE_DECLTYPES)
c = db.cursor()
c.execute('select * from unihan_dict')
cdb = {}
for code,name in c.fetchall():
  cdb[code] = re.sub(r'[|]*$', '', name)

#
# Read the list of emoji, get names from above db, and output the data
# as Javascript
#

# Current plistlib can't handle binary plists, so you need this one:
# curl -O http://bugs.python.org/file25075/plistlib.py
import plistlib
elist = plistlib.readPlist(os.path.join(apath, 'Category-Emoji.plist'))

# If verbose, prints out the list of emoji which actually displays
# them, at least in iTerm
VERBOSE = '-v' in sys.argv

categories = [];
emoji = {};
for d in elist['EmojiDataArray']:
  category = d['CVDataTitle'].replace('EmojiCategory-', '');
  categories.append(category);
  if VERBOSE: print category
  emoji[category] = []
  for rec in d['CVCategoryData']['Data'].split(','):
    if rec[:2] == '0x':
      rec = (r'\U' + rec[2:].zfill(8)).decode('unicode-escape')
    ntt = re.sub(r"u'(.*)'", r"\1", repr(rec))
    ntt = re.sub(r"\\[xuU]0*([0-9a-fA-F]*)", r"&#x\1;", ntt)
    if rec not in cdb:
      # probably a chord or whatever you might call it
      continue
    emoji[category].append((ntt, cdb[rec]))
    if VERBOSE: print rec + '  ' + cdb[rec], ntt
  if VERBOSE: print

font = elist['CVViewFontList'][0];

out = open('emoji.js', 'w')
out.write('var font = ' + repr(font) + ';\n\n');
out.write('var categories = ');
json.dump(categories, out, indent=1)
out.write(';\n\nvar emoji = ');
json.dump(emoji, out, indent=1)

